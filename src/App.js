import React from 'react';
import { Provider } from 'react-redux';

import Layout from './components/Layout';
import store from './store/store';

import './styles/app.scss'

function App() {
    return (
        <Provider store={ store }>
            <div className="App">
                <Layout/>
            </div>
        </Provider>
    );
}

export default App;