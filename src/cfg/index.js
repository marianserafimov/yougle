export default {
    google: {
        key: 'AIzaSyCMa7qU4A4fDY7p3kiQbfF0FOMxtbexw7w',
        id: '004484252824749936288:9zxaxozikg0',
    },
    
    scrollPositionEvents: 68,

    texts: {
        logo: "Yougle",
        btn_delete_tab : "Delete tab",
        btn_close_confirmation_modal : "No!",
        btn_confirm_confirmation_modal : "Yes, delete it",
        delete_confirm_text : "This operation cannot be undone. Are you sure you want to DELETE this tab?",
        search_placeholder : "Search..",
        no_image: "No image available :(",
        logo_background_click_me: "Click me!" ,
        logo_background_thanks: "Thanks!" 
    },

    types: {
        string: 'string',
        number: 'number',
        object: 'object',
        boolean: 'boolean',
        symbol: 'symbol',
        function: 'function',
    },

    buttonTypes: {
        submit: 'submit',
        button: 'button',
        reset: 'reset',
    }
};