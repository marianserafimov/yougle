import React from 'react';
import { useSelector } from 'react-redux';

import Header from './common/Header';
import Main from './base/Main';
import Footer from './common/Footer'
import Preloader from './widgets/preloader/Preloader';
import { isPreloaderLoadingSelector } from '../store/selectors'

export default function Layout() {
    const isPreloaderLoading = useSelector(isPreloaderLoadingSelector)

    return (
        <>
            <Header/>
            <Main/>
            <Footer/>
            { isPreloaderLoading && <Preloader/> }
        </>
    )
}