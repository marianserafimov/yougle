import BaseComponent from "./BaseComponent";

//Every component which is page extends this component
export default class BasePageComponent extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {

        };
    }
}