import React from 'react';

import cfg from '../../cfg';

//Every component extends this component. Its providing us basic functionalities which may be needed in every component
export default class BaseComponent extends React.Component {
    constructor(props) {
        super(props);

        this.bindFunctions = this.bindFunctions.bind(this);
    }

    // Extend services
    extends(...rest) {
        rest.forEach(obj =>{
            Object.keys(obj).forEach(name => {
                if (typeof obj[name] === cfg.types.function) {
                    this[name] = obj[name].bind(this);
                } else {
                    this[name] = obj[name];
                }
            });
        });
    }

    //Bind multiple functions easier
    bindFunctions(functions) {
        functions.forEach(func => this[func] = this[func].bind(this));
    }
}