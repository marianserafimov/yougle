import React from 'react';
import { Tabs, Tab } from "react-bootstrap";
import { useSelector } from "react-redux";
import PropTypes from 'prop-types';

import TabContent from "./TabContent";
import { tabsSelector } from '../../../store/selectors';

function TabsItems() {
    const tabs = useSelector(tabsSelector);

    return (
        <div className="tabs">
            {(!Array.isArray(tabs) || tabs.length === 0)
                ? <h2>404</h2>
                : <Tabs>
                    {tabs.map((tab) =>
                        <Tab key={ `tabs_key_${ tab.id }` } eventKey={ tab.eventKey } title={ tab.title }>
                            <TabContent id={ tab.id }/>
                        </Tab>
                    )}
                    </Tabs>
            }
        </div>
    );
}

TabsItems.propTypes = {
    tabs: PropTypes.array
};

export default TabsItems