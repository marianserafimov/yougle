import React from 'react';
import ReactPlayer from 'react-player';
import PropTypes from 'prop-types';

import BaseComponent from "../../hoc/BaseComponent";
import cfg from '../../../cfg/index';

class TabItem extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            playing: false
        };

        this.bindFunctions([
            'startStopVideo',
        ]);
    }

    startStopVideo(){
        this.setState((prevState) => ({
            playing: !prevState.playing
        }));
    }

    render() {
        const { link, img, title } = this.props;
        const { playing } = this.state;

        return (
            <div onClick={ this.startStopVideo } className={ `tab-item ${ link ? 'clickable' : '' }` }>
                <div className="image-wrapper">
                    {img
                        ? <img alt={ title } src={ img }/>
                    : <p>{ cfg.texts.no_image }</p>
                    }
                </div>
                <h3>{title}</h3>
                {link
                    && <>
                        <div className={ `black-overlay ${ playing ? 'show-overlay': ''}` }/>
                        <ReactPlayer className={ `video ${ playing ? 'show': ''}` } url={ link } style={ {width: '80%', height: '50%'} } playing={ playing } />
                    </>
                }
            </div>
        );
    }
}

TabItem.propTypes = {
    img: PropTypes.string,
    link: PropTypes.string,
    title: PropTypes.string
};

export default TabItem;