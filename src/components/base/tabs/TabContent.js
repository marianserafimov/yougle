import React from 'react';
import shortid from 'shortid';
import axios from 'axios';
import { Button, Form, InputGroup } from 'react-bootstrap';
import PropTypes from 'prop-types';

import BaseComponent from "../../hoc/BaseComponent";
import TabItem from './TabItem';
import cfg from '../../../cfg';
import store from "../../../store/store";
import { deleteTabAction } from "../../../store/actions/TabsActions";
import { openPreloaderAction, closePreloaderAction } from '../../../store/actions/PreloaderActions';
import { ReactComponent as SearchIconSvg } from '../../../assets/images/icons/search-icon.svg';
import ConfirmationModal from "../../widgets/modals/ConfirmationModal";
import ModalMethods from '../../../methods/ModalMethods'

class TabContent extends BaseComponent {
    constructor(props) {
        super(props);
        this.state = {
            query: '',
            items: false,
            modal: false,
        };

        this.bindFunctions([
            'searchClick',
            'onChangeQuery',
            'openConfirmationDelete',
            'deleteTab',
        ]);

        this.extends(
            ModalMethods.call(this),
        );
    }

    onChangeQuery(event) {
        this.setState({
            query: event.target.value
        })
    }

    deleteTab() {
        store.dispatch(deleteTabAction(this.state.deleteTabId));
    }

    openConfirmationDelete(event){
        this.openConfirmationModal();

        this.setState({
            deleteTabId: event.target.id
        });
    }

    searchClick(event) {
        event.preventDefault();

        store.dispatch(openPreloaderAction());

        let { query } = this.state;

        if (query.trim() === '') {
            store.dispatch(closePreloaderAction());
            return;
        }

        axios.get(`https://www.googleapis.com/customsearch/v1?q=${query}&cx=${cfg.google.id}&key=${cfg.google.key}`)
            .then(res => { this.setState({items: res.data.items}) })
            .catch(err => console.error('error :', {err}))
            .finally(res => store.dispatch(closePreloaderAction()));
    }

    render() {
        const { query, items, modal } = this.state;
        const { id } = this.props;
        
        return (
            <div className="search">
                <div className="top-side">
                    <Form onSubmit={ this.searchClick }>
                        <Form.Group>
                            <InputGroup>
                                <Form.Control
                                    placeholder={ cfg.texts.search_placeholder }
                                    required
                                    onChange={ this.onChangeQuery }
                                    value={ query }
                                />

                                <InputGroup.Append type={ cfg.buttonTypes.submit } onClick={this.searchClick}>
                                    <InputGroup.Text id="basic-addon2"><SearchIconSvg/></InputGroup.Text>
                                </InputGroup.Append>
                            </InputGroup>
                        </Form.Group>
                    </Form>
                    
                    <Button id={ id } onClick={ this.openConfirmationDelete }>{ cfg.texts.btn_delete_tab }</Button>
                </div>
                <div className="assets">
                    {items
                        && items.map((item) => (
                            <TabItem
                                 key={ shortid() }
                                 img={ item.pagemap.imageobject === undefined ? false : item.pagemap.imageobject[0].url }
                                 link={ item.link=== undefined ? false : item.link }
                                 title={ item.title }
                            />
                        ))
                    }
                </div>

                {modal &&
                    <ConfirmationModal
                        className="confirmation-modal"
                        onClose={ this.closeConfirmationModal }
                        onConfirm={ this.deleteTab }
                        btn1={{
                            text: cfg.texts.btn_close_confirmation_modal,
                        }}
                        btn2={{
                            text: cfg.texts.btn_confirm_confirmation_modal,
                            method: this.deleteTab,
                        }}
                    >
                        <h3>{ cfg.texts.delete_confirm_text }</h3>
                    </ConfirmationModal>
                }
            </div>
        )
    }
}

TabContent.propTypes = {
    id: PropTypes.string
};

export default TabContent;