import React from 'react'

import TabsItems from "./tabs/TabsItems";

function Main(porps) {
    return (
        <div className="main">
            <TabsItems/>
        </div>
    )
}

export default Main;