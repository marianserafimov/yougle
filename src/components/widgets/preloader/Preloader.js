import React from 'react';
import * as PropTypes from "prop-types";
import { Spinner } from 'react-bootstrap';

function Preloader({ className }) {
    return (
        <div className={ `preloader black-overlay${ className ? className : '' }` }>
            <Spinner animation="grow" />
        </div>
    );
}


Preloader.propTypes = {
    className: PropTypes.string,
};

export default Preloader;