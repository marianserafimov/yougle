import React from 'react';
import * as PropTypes from "prop-types";
import { Button } from "react-bootstrap";

import cfg from "../../../cfg";

function ConfirmationModal(props) {

    function onConfirm() {
        onClose();
        props.onConfirm();
    }

    function onClose() {
        props.onClose();
    }

    const { className, children, btn1, btn2 } = props;
    return (
        <div
            className={ `confirmation-modal ${className ? className : ''}` }
            onClick={ onClose }
        >
            <div
                className="confirmation-modal-container"
                onClick={ (event) => event.stopPropagation() }
            >
                {children}
                <div className="modal-buttons">
                    {btn1 &&
                        <Button
                            type={ cfg.buttonTypes.button }
                            onClick={ onClose }
                        >
                            { btn1.text }
                        </Button>
                    }
                    {btn2 &&
                        <Button
                            type={ btn2.method ? cfg.buttonTypes.button : cfg.buttonTypes.submit }
                            onClick={ onConfirm }
                        >
                            { btn2.text }
                        </Button>
                    }
                </div>
            </div>
        </div>
    );
}


ConfirmationModal.propTypes = {
    className: PropTypes.string,
    btn1: PropTypes.oneOfType([
        PropTypes.bool,
        PropTypes.shape({
            text: PropTypes.string,
            method: PropTypes.func,
        }),
    ]),
    btn2: PropTypes.oneOfType([
        PropTypes.bool,
        PropTypes.shape({
            text: PropTypes.string,
            method: PropTypes.func,
        }),
    ]),
};

export default ConfirmationModal;