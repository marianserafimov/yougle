import React from 'react'

import cfg from "../../cfg";

function Footer(props) {
    return (
        <div className="footer">
            <div className="copyright">&copy;{ new Date().getFullYear() } <span>{ cfg.texts.logo }</span>.com</div>
        </div>
    )
}

export default Footer;