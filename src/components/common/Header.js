import React from 'react'
import uuid from 'uuid/v4';
import shortid from 'shortid';
import { Form, Button } from 'react-bootstrap';

import BaseComponent from "../hoc/BaseComponent";
import store from '../../store/store';
import cfg from '../../cfg';
import { addTabAction } from '../../store/actions/TabsActions';

class Header extends BaseComponent {
    constructor(props) {
        super(props);
        
        this.state = {
            rotate: false,
            title: "",
            addTabWarning: false,
            scrollPosition: window.pageYOffset,
            tabName: ''
        };

        this.bindFunctions([
            'rotatingDone',
            'addTab',
            'handleScroll',
            'onChangeTabName',
        ]);
    }

    componentDidMount() {
        this.logo.addEventListener("animationend", this.rotatingDone);
        window.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnmount() {
        this.logo.removeEventListener("animationend");
        window.removeEventListener('scroll');
    }

    handleScroll() {
        this.setState({
            scrollPosition: window.pageYOffset
        })
    }

    onChangeTabName(event) {
        this.setState({
            tabName: event.target.value
        })
    }

    rotatingDone() {
        this.setState({
            rotate: false
        });
    }

    addTab(event) {
        event.preventDefault();

        const { tabName } = this.state;

        if (!tabName) {
            this.setState({
                addTabWarning: true,
            });

            return;
        }

        if(tabName.trim() === '') {
            this.setState({
                addTabWarning: true,
                tabName: '',
            });
        } else {
            let data = {
                id: uuid(),
                eventKey: shortid(),
                title: tabName
            };

            this.setState({
                addTabWarning: false,
                tabName: '',
            }, () => store.dispatch(addTabAction(data)));
        }
    }

    render() {
        const { scrollPosition, rotate, tabName, addTabWarning, isLogoClicked } = this.state;

        return (
            <>
                <div className={ `scrolled-div ${ scrollPosition > cfg.scrollPositionEvents ? 'show' : '' }` }/>
                <div className="header">
                    <h1
                        ref={elm => {
                            this.logo = elm
                        }}
                        className={ rotate ? 'rotate ' : '' }
                        onClick={ () => this.setState({ rotate: true, isLogoClicked: true }) }
                    >
                        <span>{ isLogoClicked ? cfg.texts.logo_background_thanks : cfg.texts.logo_background_click_me }</span>
                        { cfg.texts.logo }
                    </h1>

                    <Form
                        onSubmit={ this.addTab }
                        className={ scrollPosition > cfg.scrollPositionEvents ? 'scrolled' : '' }
                    >
                        <Form.Group>
                            <Form.Control
                                onChange={ this.onChangeTabName }
                                value={ tabName }
                                type="text"
                                placeholder="Add a tab"
                                maxLength="30"
                                className={ addTabWarning ? 'warning' : '' }
                            />
                        </Form.Group>

                        <Button type="submit">&#43;</Button>
                    </Form>
                </div>
            </>
        )
    }
}

export default Header;