import { ADD_TAB, DELETE_TAB } from '../types';

export const addTabAction = (tab) => ({
    type: ADD_TAB,
    payload: tab
});

export const deleteTabAction = (tabId) => ({
    type: DELETE_TAB,
    payload: tabId
});