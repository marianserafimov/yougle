import { OPEN_PRELOADER, CLOSE_PRELOADER } from '../types';

export const openPreloaderAction = () => ({
    type: OPEN_PRELOADER,
});

export const closePreloaderAction = () => ({
    type: CLOSE_PRELOADER,
});