import { combineReducers } from 'redux';

import TabsReducer from './TabsReducer';
import PreloaderReducer from './PreloaderReducer';

const rootReducer = combineReducers({
    tabs: TabsReducer,
    preloader: PreloaderReducer
});

export default rootReducer;
