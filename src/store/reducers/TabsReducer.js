import { ADD_TAB, DELETE_TAB } from '../types';

import uuid from 'uuid';
import shortid from 'shortid';

const INITIAL_STATE =  [
                            {
                                id: uuid(),
                                eventKey: shortid(),
                                title: 'Default'
                            }
                        ];

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ADD_TAB:
            return [ ...state, action.payload ];
            
        case DELETE_TAB:
            return [ ...state.filter((tab) => tab.id !== action.payload) ];

        default:
            return state;
    }
}