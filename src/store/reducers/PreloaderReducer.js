import { OPEN_PRELOADER, CLOSE_PRELOADER } from '../types';

export default (state = false, action) => {
    switch (action.type) {
        case OPEN_PRELOADER:
            return state= true;
        case CLOSE_PRELOADER:
            return state= false;
        default:
            return state;
    }
}