import { path } from 'ramda';

export const isPreloaderLoadingSelector = path(['preloader']);