export default function() {
    return {
        openConfirmationModal() {
            this.setState({ modal: true });
        },

        closeConfirmationModal() {
            this.setState({ modal: false });
        }
    }
};