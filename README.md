## Installation

- All the `code` required to get started
- Images of what it should look like

### Clone

- Clone this repo to your local machine using `https://gitlab.com/marianserafimov/yougle.git`

### Setup
> now install npm packages

```shell
$ npm install
```
> then run the application

```shell
$ npm start
```

### Exercise:
Create an application that lists videos and images. 

This is tab based application. Every tab consist of the same UI:

- It must contain an input field that is used to search for the assets and a grid that shows them. 
- Each tile in the grid has a thumbnail and a title (can be filename).
- The actual assets are to be retrieved using YouTube API and Google Custom Search Engine API.
- Clicking on an asset should popup a dialog or just enlarge it to fill the window. 
- The state of each tab should be preserved, so that the user can switch between tabs without losing any data. (Think of the way that the browser works)
- Be able to open new tab – it could be “+” button similarly to browsers
- Be able to close existing tabs

> BONUS: Desing the application

> BONUS: Use CSS preprocessor (Less, Sass), Bootstrap or Material Design

> BONUS: Inplement preloader

Upload the app in GitHub account of your preference and provide the URL 

Use React to fulfill the task


- Copyright Date.now()© Yougle.
